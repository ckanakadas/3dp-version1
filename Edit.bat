@echo off
set /p var1=<D:/filename.txt
D:\MeshLab\meshlab.exe "C:\3d\%var1%.stl"
pause
set LOGFILE=D:/%var1%-edit.log
call :LOG >> %LOGFILE%
exit
:LOG
D:\virtual\slic3r\slic3r.exe	"C:\3d\%var1%.stl" --layer-height 0.2 --output C:/temp-3d/%var1%-v1.gcode
D:/crypt.exe -encrypt -key D:/key.txt -infile C:/temp-3d/%var1%-v1.gcode -outfile C:/3d/%var1%-v1-encoded.gcode
set root=c:\3d
cd  /d %root%
git pull https://ckanakadas@bitbucket.org/CTO-Innovators/3d.git master
git status
git add --all .
git commit -m "Test commit"
git push https://ckanakadas@bitbucket.org/CTO-Innovators/3d.git master
D:/crypt.exe -decrypt -key D:/key.txt -infile C:/3d/%var1%-v1-encoded.gcode -outfile d:\dec\%var1%-v1-decoded.gcode
echo state > D:\Actual\%var1%.txt
 "C:\Program Files\Java\jre1.8.0_111\bin\java.exe" -jar D:\virtual\GCodeSimulatorV132.jar d:\dec\%var1%-v1-decoded.gcode
del "d:\dec\%var1%-v1-decoded.gcode"
del "D:/Actual\%var1%.txt"