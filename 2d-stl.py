from scipy.misc import lena
from pylab import imread
from scipy.ndimage import gaussian_filter
from stl_tools import numpy2stl, text2png


"""
Some quick examples
"""

A = 256 * imread("examples/gear.PNG")
A = A[:, :, 2] + 1.0*A[:,:, 0] # Compose RGBA channels to give depth
A = gaussian_filter(A, 1)  # smoothing
numpy2stl(A, "examples/gear.stl", scale=0.05, mask_val=5., solid=True)
