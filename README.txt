                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:44286
GCode Print Simulator by mdietz is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

This application can visualize Gcodes and simulate a real 3D print.  
GCode Simulator recognizes the print speeds and can simulate the print in realtime, but you can also speedup the prints (fast forward,slow motion). Each layer is painted in a different color to see how a layer overlaps with the layer below.  

GCodeSimulator Home page: http://3dprintapps.de/  

Video of GCodeSimulator in action: http://www.youtube.com/watch?v=i1SY2N21EDQ  

GCodeSimulator let you to check how your print should turn out, making it easier to spot errors and fix them instead of wasting filament. If the print object looks ok you can directly send it over Wifi to GCodePrintr (Android) to print it.   
**GCodePrintr  - 3D Printing for ANDROID available**  
**Allows real 3D printing from an Android device**  
https://play.google.com/store/apps/details?id=de.dietzm.gcodesimulator  

In addition GCodeSimulator analyses the Gcode file and prints additional information like:  
It calculates various print details like  
-print time  
-used filament  
-yx move distance  
-print object dimension  
-average print speeds  
-number of layers  
-layer details  
-speed distribution / Acceleration  
-weight and price of the printed object  
-much more ...  

It also allows to debug your gcode by stepping through it line by line and show the results. (debug mode)  

Developed for my reprap printer but works with other printers (e.g. makerbot, ultimaker,...) as well, tested with Silc3r and skeinforge gcodes.  

**Download the latest Version V130**  


# Instructions

Add GCode Simulator as post-processing script in Slic3r !!  

**GCode Simulator for ANDROID available**  
**Allows real 3D printing from an Android device (not just simulation)**  
https://play.google.com/store/apps/details?id=de.dietzm.gcodesimulator   

**Update V1.32**  
Fix for G02/G03 gcodes
Fit to page zoom
more fixes 

**Update V1.30**  
Improved behavior when jumping between layers  
Support for G00,G01,G02 gcodes  
Thumbnail support - can be integrated with Nautilus  


**Update V1.29**  
New Snapshot Image function to create a snapshot of the model and store it as jpg. Helps to get an overview and categorize your existing gcodes.   
Round print bed support  
Antialiasing   
Many fixes   

**Update V1.27**  
Option to autostart print when sending gcode to GcodePrintr  (>=V1.67)  
Option to autosave model when sending gcode to GcodePrintr  (>=V1.67)  
Option to configure offset for dual extruder  
Many bug fixes  

**Update V1.25**  
    Added limited support for BfB style gcodes  
    Added "Jump to Layer" option  
    New button bar & banner  
    Fixed nozzle offset  

**Update V1.22**  
Configurable bed size  
Gray and Autumn Themes  


**Update V1.21**  
New Button bar   
Layout changed  
New print panel (not working yet)  

**Update V1.18**  
Fix wrong label painting  
Remember Network IP  
Remember last file path  
**Update V1.17**  
Many performance improvments  
GCode Parser improvements  
Paint Extruder on frotn view  
Workaround for MacOS load bug   

**Update V1.16**  
Fixed problem when parsing gcodes with multiple whitespaces  

**UPDATE V1.15**  
Fixed NullPointerException with G4 gcode  

**UPDATE V1.14**  
Fixed Network Send Feature (broken in V1.12)  
Improved load time  

**UPDATE V1.12**  
Add makeware specific extrusion code ( A&B instead of E)  
Removed debug message which slowed down the load   
Fixed side view, slicer comments view, 1st layer temp  
Removed experimental edit mode (speek up if you need it!)  

**UPDATE V1.11**  
Add makeware specific extrusion code ( A instead of E)  
Fixed boundary calculation (start position was ignored)  

**UPDATE V1.10**  
Improved performance  
Significantly reduced memory usage  

**UPDATE V1.07**  
-New Network Send (Remote Print) Feature - allows you to send the Gcode to an Android device running Gcode Simulator V1.05 Beta.  
-Added new menu items for the functions added in V1.02  
-Fixed Z-lift Bug (layers were reordered by Z-pos)  


**UPDATE V1.03-V1.06**  
Various updates to the Android Beta Version  

**UPDATE V1.02**  
Fixed some ReplicatorG Bugs  
Fixed some "Invalid Gcode" errors by adding support for addtl. Gcodes (M204,M70,G161,...)  
Show current Gcode line when in pause (debug mode)  
Added step by step execution (debug mode). Use space/backspace to step through the gcodes during pause  
Added fast forward/rewind (skip 50 gcodes). Use space/backspace during run.  
Added increase/decrease of speedup in larger steps (10x). Use / (Up) * (Down)  

**UPDATE V0.99**  
The current location of the nozzle is painted to improve usability.  
Replaced blue with a lighter pink to improve readability.  
Click on speedup label box will toggles pause  
Scrollwheel for speedup is only active when mouse is over printbed or labels  
**UPDATE V0.98**  
Support for center format G2/G3 Gcodes (Arc) - Radius format not supported yet  
**UPDATE V0.97**  
More reslilient against errors and unsupported gcodes  
**UPDATE V0.96**  
Display extrusion rate (mm/min) in real time  
Display pause status in status bar  
Display of Fan output changed (III=Full Speed, II=Medium, I=Slow, ""=off)  
Rendering bug (Grey spots) fixed  

**UPDATE V0.95**  
Based on new GCodeInfo   
Experimental Edit Mode added  
Optimize lavel repaint  
Fixed bugs with details toggle and temperature   

**UPDATE V0.93**  
Some performance improvements.   
Window Icon added.   
Automatic Zoom on Window resize.  
Filedialog uses current user path.  
Fixed some multi-threading bugs.   

**UPDATE V0.92**  
Added Menubar   
Show model details by default.   
Fixed double buffering bug for MAC OS.   
Some Performance improvements  

**UPDATE V0.91**  
Show current speed and remaining print time.   
Fixed double buffering bug.  


**UPDATE V0.90**  
Render Front and Side View  
Experimental Printing support (requires RXTXcomm.jar and /dev/ttyUSB0)   
Added S Key-Shortcut for "Start Print"   
Some Bug Fixes  

**UPDATE V0.81**  
Added Mouse Support   
                Mousewheel = Speed up/down  
		ALT+Mousewheel = Zoom in/out  
		Left Button on Bed = Next Layer  
		ALT+Left Button on Bed = Previous Layer  
		Right Button = Show Model Details  
		Left Button on Details = Toggle Model Details  
		Middle Button = Show Help  
Show percentage when loading gcode files  
Start with default gcode (GCode Simulator Key Chain) , press f to load file.  
Fixed bugs.   


**UPDATE V0.80**  
Much smoother painting by splitting longer lines into multiple draws  
Added nice looking labels with boxes for current infos (instead of plain test string)  
Toggle modeldetails can now page through multiple pages of layer details  
Unknown keys open about/help dialog.  
Fixed acceleration (ignore acceleration for speed distribution), use acceleration for paint and layer time.   
Show separate time values (with and without acceleration)  

**UPDATE V0.71**  
Non-disruptive zoom (no restart) implemented  
Jump to previous layer added.   
"Next Layer" option completes the layer painting (not skips it)  
Paint grid on print bed.  

**UPDATE V0.69**  
Show new GCodeInfo modeldetails (Material,Weight,Price,.. See GcodeInfo for details.)   
Fixed some issues with Skeinforge comments and average calulations.  

**UPDATE V0.68**  
Support for Java Version 1.6 (MacOS)  

**UPDATE V0.67**  
Added option (t) to toggle details (show more model & layer details similar to Gcodeinfo)  
Added option (f) to open another gcode file.   

**UPDATE V0.65**  
Handle gcode files with Z-Lifts and negative coordinates (auto center on bed).   
Show details of the printed model (e.g. overall print time, size, layers,..)  


**How to start GCodeSimulator on Windows/Linux/MacOS**
GCodeSimulator is written in Java and therefore requires a Java Runtime Environment (>=1.6) to be installed on the System. You can get the Java JRE from www.java.com (Oracle). 
Once Java is installed you can start GCodeSimulator by double click on the GCodeSimulator.jar file. (Open with java from explorer or your favorite file browser.)

If this does not work, please start GCodeSimulator by calling java from the command line 
   java -jar GCodeSimulator.jar [gcode_file]

[gcode_file] is optional , if not specified then a browse dialog will open.  

IMPORTANT: Due to a bug in the linux java jre do not choose a file from the "recent file" list, this will cause a crash. Instead go to the full path of the file and open it there.   

The GCode Print simulator can be controlled with the following hotkeys:  
 i/o=Zoom   
 +/-=Speed   
 n=NextLayer   
 r=Restart   
 p=Pause   
 q=Quit  

I have tested it with many Slic3r gcodes and some skeinforge gcodes. But GCodes can be very complex, therefore I'm pretty sure there will still be bugs.   
Z-Lifts might causes problems and negative coordinates as well (fixed with V0.65). Relative positioning is not supported yet.   
Feedback is highly welcome !